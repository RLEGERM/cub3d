/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_cast.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rleger <rleger@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/19 23:22:34 by rleger            #+#    #+#             */
/*   Updated: 2023/11/20 23:21:13 by rleger           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <stdio.h>
#include <math.h>

void	set_incr(double angle, int *incr_x, int *incr_y)
{
	if ((angle == M_PI || angle == 2 * M_PI))
		*incr_x = (angle - 2 * M_PI + 1 ) / fabs(angle - 2 * M_PI + 1);
	else if (angle == M_PI / 2 || angle == 3 * M_PI / 2)
		*incr_y =(M_PI / 2 - angle + 1) / fabs(M_PI / 2 - angle + 1);
	else if ( angle < M_PI / 2)
	{
		*incr_x = 1;
		*incr_y = 1;
	}
	else if (angle < M_PI)
	{
		*incr_x = -1;
		*incr_y = 1;
	}
	else if (angle < 3 / 2 * M_PI)
	{
		*incr_x = -1;
		*incr_y = -1;
	}
	else
	{
		*incr_x = 1;
		*incr_y = -1;
	}
}

double	ray_dist(double x0, double y0, double x1, double y1)
{
	return (sqrt(pow(x1 - x0, 2) + pow(y1 - y0, 2)));
}

double	ray_collisiont(double x0, double y0, double angle, t_player *pov)
{
	double	m;
	double	p;
	int		incr_x;
	int		incr_y;
	char	side;

	incr_x = 0;
	incr_y = 0;
	set_incr(angle, &incr_x, &incr_y);
	m = tan(angle);
	p = y0 - x0 * m;
	while (!pov->scene->map[(int)x0][(int)y0])
	{
		if (!incr_y || m * ((int)x0 + incr_x) + p <= (int)y0 + incr_y)
		{
			x0 = (int)x0 + incr_x;
			side = 'x';
		}
		else
		{
			y0 = (int)y0 + incr_y;
			side = 'y';
		}
	}
	if (side == 'x' && incr_x == 1)
		side = 'W';
	else if (side == 'x' && incr_x == -1)
		side = 'E';
	else if (incr_y == 1)
		side = 'N';
	else
		side = 'S';
	return (ray_dist(pov->x_pos, pov->y_pos, x0, y0));	
}

int main()
{
	return (ray_collisiont(0.5, 0.5, M_PI * 225/ 180));
}