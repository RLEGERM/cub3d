/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_image.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rleger <rleger@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 23:05:26 by rleger            #+#    #+#             */
/*   Updated: 2023/11/20 16:46:25 by rleger           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	my_mlx_pixel_put(t_data *data, int x, int y, int color)
{
	char	*dst;

	dst = data->addr + (y * data->line_length + x * (data->bits_per_pixel / 8));
	*(unsigned int *)dst = color;
}

void	draw_img(t_params *params)
{
	int			color_int;
	int			x;
	int			y;

	x = -1;
	while (++x < HEIGHT)
	{
		y = -1;
		while (++y < WIFHT)
		{

			color_int = 12;
			my_mlx_pixel_put(params->img, y, x, (1 - ((double)color_int)));
		}
	}
	mlx_put_image_to_window(params->mlx_ptr, params->mlx_win_ptr,
		params->img->img, 0, 0);
}
