/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rleger <rleger@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/11/17 22:34:21 by rleger            #+#    #+#             */
/*   Updated: 2023/11/20 21:45:20 by rleger           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef CUB3D_H
#define CUB3D_H

# include "../mlx/mlx.h"
# include <math.h>

# ifndef HEIGHT
#  define HEIGHT 360
# endif
# ifndef WIDTH
#  define WIDTH 480
# endif
# ifndef FOV
#  define FOV M_PI * 5 / 6 // 150°
# endif

typedef struct s_color
{
	int	r;
	int	g;
	int	b;
}				t_color;


typedef struct s_data
{
	void		*img;
	char		*addr;
	int			bits_per_pixel;
	int			line_length;
	int			endian;
}				t_data;

typedef struct s_params
{
	void		*mlx_ptr;
	void		*mlx_win_ptr;
	int			color;
	t_data		*img;
}				t_params;

typedef struct	s_scene
{
	char		*n_text;
	char		*s_text;
	char		*e_text;
	char		*w_text;
	t_color		floor;
	t_color		ceilling;
	char		**map;
	t_player	*player;
	int			x_max;
	int			y_max;
}				t_scene;

typedef struct s_player
{
	t_scene		*scene;
	double		x_pos;
	double		y_pos;
	double		angle;
	
}				t_player;

void	my_mlx_pixel_put(t_data *data, int x, int y, int color);
void	draw_img(t_params *params);

#endif