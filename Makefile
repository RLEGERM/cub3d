# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rleger <rleger@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/11/17 22:33:54 by rleger            #+#    #+#              #
#    Updated: 2023/11/19 23:38:26 by rleger           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = cub3d
UNAME_S := $(shell uname -s)

CC = gcc
CFLAGS = -Wall -Wextra -Werror -g3

ifeq ($(UNAME_S),Darwin)
	MLXFLAGS = -Lmlx -lmlx -framework OpenGL -framework AppKit
else
    MLXFLAGS = -Lmlx -lmlx -L/usr/lib -Imlx -lXext -lX11 -lm -lz
endif

AR = ar rcs
RM = rm -f
HEAD = includes/

HEADER = includes/cub3d.h \

SRCS = main.c \
		src/draw_image.c \
		src/ray_cast.c

OBJ_DIR = obj
OBJS = $(patsubst %.c,$(OBJ_DIR)/%.o,$(SRCS))
	
$(OBJ_DIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -I $(HEAD) -Imlx -c $< -o $@
	
$(NAME): $(OBJS) $(HEADER)
	$(MAKE) -C mlx
	$(CC) $(CFLAGS) -I $(HEAD) $(OBJS) $(MLXFLAGS) -o $(NAME)

all: $(NAME)

clean:
	$(MAKE) clean -C mlx
	$(RM) -r $(OBJ_DIR)

fclean: clean
	$(RM) $(NAME)

re: clean all

.PHONY: all clean fclean re
